import { Config } from 'tailwindcss'

/** @type {import('tailwindcss').Config} */

export default <Partial<Config>>{
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  content:[
    `./components/**/*.{vue,js,ts}`,
  ]
}

