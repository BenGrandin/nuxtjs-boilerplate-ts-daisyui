import { defineNuxtConfig } from 'nuxt/config'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  components: [
    { path: '~/components/atom', pathPrefix: false },
    '~/components',
  ],
  devtools: { enabled: true },
  modules: [ '@nuxtjs/tailwindcss' ],
  typescript: {
    strict: true,
  },
})
